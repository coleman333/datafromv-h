const https = require('https');
const fs = require("fs");
const path = require("path");
const async = require('promise-async');
const _ = require('lodash');

const products = 'https://v-h.com.ua/produkty.html';
const Installation = 'https://v-h.com.ua/montazh-kholodilnogo-oborudovaniya.html';
const technology = 'https://v-h.com.ua/tekhnologii-khraneniya.html';

// function request(options) {
//     return new Promise((resolve, reject)=> {
//         const req = https.request(options, (res) => {
//             let content = '';
//             res.on('data', (chunk) => {
//                 content += chunk;
//             });
//             res.on('end', () => {
//                 resolve(content);
//             });
//         });
//
//         req.on('error', reject);
//
//         req.end();
//     });
// }
//
// // comjshop
// request({
//     hostname: 'v-h.com.ua',
//     path: '/produkty.html',
//     method: 'GET'
// })
//     .then(content=>{
//         const parser = new DOMParser();
//         const dom= parser.parseFromString(content, "text/xml");
//         console.log(`\n\n=>>`,dom)
//
//     })
//     .catch(err=>console.error(err));

const {JSDOM} = require('jsdom');
const cheerio = require('cheerio');

function parse(url) {
    return JSDOM.fromURL(url)
        .then(dom => {
            const $ = cheerio.load(dom.serialize());
            const urls = $(`.jshop_list_category a[href^="/produkty/"],
               .jshop.list_category a[href^="/produkty/"],
               #comjshop_list_product .name .product_title a[href^="/produkty/"]`)
                // .eq(0)
                // .slice(0, 2)
                .map(function (index, product) {
                    return {
                        name: _.trim($(this).text()),
                        url: `https://v-h.com.ua${$(this).attr('href')}`
                    };
                });

            const productElem = $(`#comjshop.productfull`);
            if (productElem.length) {

                // const arr = $('header.uk-comment-header');
                // const results = [];
                // for (let i = 0; i < arr.length; i++) {
                //     const elem = arr[i];
                //
                //     const obj = {
                //         name: $('h4[itemprop="author"]', $(elem)).text(),
                //         date: $(arr[i]).find('span.uk-comment-meta').text(),
                //         avatar: `https://v-h.com.ua` + $(elem).find('img.uk-comment-avatar').attr('src'),
                //         review: $(elem).find('div[itemprop="reviewBody"].review_text').text()
                //         // $(this).text();
                //     };
                //
                //     results.push(obj);
                // }

                return {
                    name: $(`span[itemprop="name"]`).text(),
                    price: $(`span[itemprop="price"]`).text(),
                    img: $(`img.uk-thumbnail`).attr('src'),       //getAttribute(src)
                    videos: $(`div.video_full iframe`).map(function () {
                        return $(this).attr('src');
                    }).get(),
                    weight: $(`#block_weight`).text(),
                    visits: _.chain($(`[itemprop="aggregateRating"] .uk-icon-eye`).parent().text()).trim().toNumber().value(),
                    // reviews: results
                    reviews: $('header.uk-comment-header')
                        .map(function (index,elem) {
                            return {
                                name: $('h4[itemprop="author"]', $(this)).text(),
                                date: $(this).find('span.uk-comment-meta').text(),
                                avatar: `https://v-h.com.ua` + $(this).find('img.uk-comment-avatar').attr('src'),
                                review: $(this).find('div[itemprop="reviewBody"].review_text').text()
                                // $(this).text();
                            };
                        }).get()
                };
            }

            return async.mapSeries(
                urls,
                (data, callback)=> {
                    console.log(data.name, '=>>', data.url);
                    return parse(data.url)
                        .then(res=> {
                            if (_.isArray(res)) {
                                return callback(null, {group: data.name, url: data.url, records: res})
                            }
                            callback(null, res)
                        })
                        .catch(callback)
                }
            )
                .then((records)=> {
                    const url = $(`div.pagination li.pagination-next a`).attr('href');
                    // console.log(`\n\n=>>`, url)
                    if (url) {
                        return parse(`https://v-h.com.ua${url}`)
                            .then(innerRecords=>_.concat(records, innerRecords));
                    }
                    return records;
                })
        })
}

parse(products)
    .then((data)=> {
        return new Promise((resolve, reject)=> {
            fs.writeFile(path.join(__dirname, "products.json"), JSON.stringify(data), function (error) {
                if (error) throw error;
                resolve();
            })
        })
    })
    .then(()=>console.log('done'))
    .catch(console.error.bind(console));


//
// function parseInstalling() {
//
//     const urls = $('.categorylist_header_title a[href^="/montazh-kholodilnogo-oborudovaniya/"]');
//         // .eq(0)
//         // .slice(0, 2)
//         urls.map(function (index, product) {
//             return {
//                 name: _.trim($(this).text()),
//                 url: `https://v-h.com.ua${$(this).attr('href')}`
//             };
//         });
//     console.log(1234,url)
// }
// parseInstalling();
//


function parse2(url) {
    return JSDOM.fromURL(url)
        .then(dom => {
            const $ = cheerio.load(dom.serialize());
            const urls = $('a[href^="/montazh-kholodilnogo-oborudovaniya/"]')
                // .eq(0)
                // .slice(0, 2)
                .map(function (index, product) {
                    // console.log(urls);
                    return {
                        name: _.trim($(this).text()),
                        url: `https://v-h.com.ua${$(this).attr('href')}`
                    };
                });

            const installElem = $(`.uk-article-title`);
            if (installElem.length) {
                return {
                    // title: $(`h1.uk-article-title`).map(function () {
                    //          return _.trim($(this).text()); }).get(),
                    title:_.trim($(`h1.uk-article-title`).text()),
                    img: $(`img[src^="/images/"]`).map(function () {
                        return 'https://v-h.com.ua'+$(this).attr('src'); }).get(),
                    content:$(`p`).map(function () {
                        return _.trim($(this).text()); }).get()
                };
            }

            return async.mapSeries(
                urls,
                (data, callback)=> {
                    console.log(data.name, '=>>', data.url);
                    return parse2(data.url)
                        .then(res=> {
                            if (_.isArray(res)) {
                                return callback(null, {group: data.name, url: data.url})
                            }
                            callback(null, res)
                        })
                        .catch(callback)
                }
            )
        })
}

parse2(Installation)
    .then((data)=> {
        return new Promise((resolve, reject)=> {
            fs.writeFile(path.join(__dirname, "installation.json"), JSON.stringify(data), function (error) {
                if (error) throw error;
                resolve();
            })
        })
    })
    .then(()=>console.log('done installation'))
    .catch(console.error.bind(console));



function parse3(url) {
    return JSDOM.fromURL(url)
        .then(dom => {
            const $ = cheerio.load(dom.serialize());
            const urls = $('a[href^="/tekhnologii-khraneniya/"]')
            // .eq(0)
            // .slice(0, 2)
                .map(function (index, product) {
                    // console.log(urls);
                    return {
                        name: _.trim($(this).text()),
                        url: `https://v-h.com.ua${$(this).attr('href')}`
                    };
                });

            const installElem = $(`.uk-article-title`);
            if (installElem.length) {
                return {
                    // title: $(`h1.uk-article-title`).map(function () {
                    //     return _.trim($(this).text()); }).get(),
                    title:_.trim($(`h1.uk-article-title`).text()),
                    img: $(`img[src^="/images/"]`).map(function () {
                        return 'https://v-h.com.ua'+$(this).attr('src');  }).get(),
                    content:$(`p`).map(function () {
                        return _.trim($(this).text()); }).get()
                };
            }

            return async.mapSeries(
                urls,
                (data, callback)=> {
                    console.log(data.name, '=>>', data.url);
                    return parse3(data.url)
                        .then(res=> {
                            if (_.isArray(res)) {
                                return callback(null, {group: data.name, url: data.url})
                            }
                            callback(null, res)
                        })
                        .catch(callback)
                }
            )
        })
}

parse3(technology)
    .then((data)=> {
        return new Promise((resolve, reject)=> {
            fs.writeFile(path.join(__dirname, "technology.json"), JSON.stringify(data), function (error) {
                if (error) throw error;
                resolve();
            })
        })
    })
    .then(()=>console.log('done technology'))
    .catch(console.error.bind(console));